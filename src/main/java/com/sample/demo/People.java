package com.sample.demo;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class People {

    @Id
    private int ID;
    private String firstName;
    private String lastName;
    private String career;

    public People() {
    }

    public People(int ID, String firstName, String lastName, String career) {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.career = career;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }
}
