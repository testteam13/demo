package com.sample.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PeopleService {

    @Autowired
    private PeopleRepository peopleRepository;

    public List<People> getAllPeople(){
        List<People> peopleList = new ArrayList<>();
        peopleRepository.findAll().forEach(peopleList::add);
        return peopleList;
    }
}
