DROP TABLE IF EXISTS billionaires;
DROP TABLE IF EXISTS people;

CREATE TABLE billionaires (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  career VARCHAR(250) DEFAULT NULL
);

INSERT INTO billionaires (first_name, last_name, career) VALUES
  ('Aliko', 'Dangote', 'Billionaire Industrialist'),
  ('Bill', 'Gates', 'Billionaire Tech Entrepreneur'),
  ('Folrunsho', 'Alakija', 'Billionaire Oil Magnate');

  CREATE TABLE people (
    id INT AUTO_INCREMENT  PRIMARY KEY,
    first_name VARCHAR(250) NOT NULL,
    last_name VARCHAR(250) NOT NULL,
    career VARCHAR(250) DEFAULT NULL
  );

  INSERT INTO people (first_name, last_name, career) VALUES
    ('Aliko', 'Dangote', 'Industrialist'),
    ('Bill', 'Gates', 'Tech Entrepreneur'),
    ('Folrunsho', 'Alakija', 'Oil Magnate');